class AddDistrictToPageContacts < ActiveRecord::Migration
  def change
    add_column :page_contacts, :district, :string
  end
end
