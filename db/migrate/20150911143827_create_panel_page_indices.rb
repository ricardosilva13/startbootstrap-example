class CreatePanelPageIndices < ActiveRecord::Migration
  def change
    create_table :panel_page_indices do |t|
      t.string :title_panel
      t.text :body_panel
      t.string :link_button
      t.string :link_visible

      t.timestamps null: false
    end
  end
end
