class CreatePageContacts < ActiveRecord::Migration
  def change
    create_table :page_contacts do |t|
      t.string :patio
      t.string :phone
      t.string :email

      t.timestamps null: false
    end
  end
end
