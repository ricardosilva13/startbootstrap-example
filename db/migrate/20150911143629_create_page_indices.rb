class CreatePageIndices < ActiveRecord::Migration
  def change
    create_table :page_indices do |t|
      t.string :image_one
      t.string :image_two
      t.string :image_three
      t.string :image_four

      t.timestamps null: false
    end
  end
end
