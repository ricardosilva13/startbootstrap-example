require 'rails_helper'

RSpec.describe PanelPageIndex, type: :model do
  it{expect(subject).to validate_presence_of(:title_panel)}
  it{expect(subject).to validate_presence_of(:body_panel)}

  it 'has factory valid' do
    expect(create(:panel_page_index)).to be_valid
  end

  context "is invalid" do
    it "without title_panel" do
      panel_page_index = build(:panel_page_index, title_panel: nil)
      panel_page_index.valid?
      expect(panel_page_index.errors[:title_panel]).to include("can't be blank")
    end
  end

  context "is invalid" do
    it "without body_panel" do
      panel_page_index = build(:panel_page_index, body_panel: nil)
      panel_page_index.valid?
      expect(panel_page_index.errors[:body_panel]).to include("can't be blank")
    end
  end

end
