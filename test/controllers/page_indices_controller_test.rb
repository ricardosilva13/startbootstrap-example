require 'test_helper'

class PageIndicesControllerTest < ActionController::TestCase
  setup do
    @page_index = page_indices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:page_indices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create page_index" do
    assert_difference('PageIndex.count') do
      post :create, page_index: {  }
    end

    assert_redirected_to page_index_path(assigns(:page_index))
  end

  test "should show page_index" do
    get :show, id: @page_index
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @page_index
    assert_response :success
  end

  test "should update page_index" do
    patch :update, id: @page_index, page_index: {  }
    assert_redirected_to page_index_path(assigns(:page_index))
  end

  test "should destroy page_index" do
    assert_difference('PageIndex.count', -1) do
      delete :destroy, id: @page_index
    end

    assert_redirected_to page_indices_path
  end
end
