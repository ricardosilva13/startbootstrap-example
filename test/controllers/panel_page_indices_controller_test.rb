require 'test_helper'

class PanelPageIndicesControllerTest < ActionController::TestCase
  setup do
    @panel_page_index = panel_page_indices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:panel_page_indices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create panel_page_index" do
    assert_difference('PanelPageIndex.count') do
      post :create, panel_page_index: {  }
    end

    assert_redirected_to panel_page_index_path(assigns(:panel_page_index))
  end

  test "should show panel_page_index" do
    get :show, id: @panel_page_index
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @panel_page_index
    assert_response :success
  end

  test "should update panel_page_index" do
    patch :update, id: @panel_page_index, panel_page_index: {  }
    assert_redirected_to panel_page_index_path(assigns(:panel_page_index))
  end

  test "should destroy panel_page_index" do
    assert_difference('PanelPageIndex.count', -1) do
      delete :destroy, id: @panel_page_index
    end

    assert_redirected_to panel_page_indices_path
  end
end
