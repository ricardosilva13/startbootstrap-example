class PanelPageIndex < ActiveRecord::Base
  validates :title_panel, :body_panel ,  presence: true
end
