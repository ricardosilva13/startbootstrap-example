json.array!(@panel_page_indices) do |panel_page_index|
  json.extract! panel_page_index, :id
  json.url panel_page_index_url(panel_page_index, format: :json)
end
