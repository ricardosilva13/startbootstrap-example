json.array!(@page_indices) do |page_index|
  json.extract! page_index, :id
  json.url page_index_url(page_index, format: :json)
end
