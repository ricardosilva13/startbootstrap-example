class PageIndicesController < ApplicationController
  layout 'admin'
  before_action :set_page_index, only: [:show, :edit, :update, :destroy]

  # GET /page_indices
  # GET /page_indices.json
  def index
    @page_indices = PageIndex.all
  end

  # GET /page_indices/1
  # GET /page_indices/1.json
  def show
  end

  # GET /page_indices/new
  def new
    @page_index = PageIndex.new
  end

  # GET /page_indices/1/edit
  def edit
  end

  # POST /page_indices
  # POST /page_indices.json
  def create
    @page_index = PageIndex.new(page_index_params)

    respond_to do |format|
      if @page_index.save
        format.html { redirect_to @page_index, notice: 'Page index was successfully created.' }
        format.json { render :show, status: :created, location: @page_index }
      else
        format.html { render :new }
        format.json { render json: @page_index.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /page_indices/1
  # PATCH/PUT /page_indices/1.json
  def update
    respond_to do |format|
      if @page_index.update(page_index_params)
        format.html { redirect_to @page_index, notice: 'Page index was successfully updated.' }
        format.json { render :show, status: :ok, location: @page_index }
      else
        format.html { render :edit }
        format.json { render json: @page_index.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /page_indices/1
  # DELETE /page_indices/1.json
  def destroy
    @page_index.destroy
    respond_to do |format|
      format.html { redirect_to page_indices_url, notice: 'Page index was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page_index
      @page_index = PageIndex.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_index_params
      params[:page_index]
    end
end
