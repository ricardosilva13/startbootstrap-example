class PanelPageIndicesController < ApplicationController
  layout 'admin'
  before_action :set_panel_page_index, only: [:show, :edit, :update, :destroy]

  # GET /panel_page_indices
  # GET /panel_page_indices.json
  def index
    @panel_page_indices = PanelPageIndex.all
  end

  # GET /panel_page_indices/1
  # GET /panel_page_indices/1.json
  def show
  end

  # GET /panel_page_indices/new
  def new
    @panel_page_index = PanelPageIndex.new
  end

  # GET /panel_page_indices/1/edit
  def edit
  end

  # POST /panel_page_indices
  # POST /panel_page_indices.json
  def create
    @panel_page_index = PanelPageIndex.new(panel_page_index_params)

    respond_to do |format|
      if @panel_page_index.save
        format.html { redirect_to @panel_page_index, notice: 'Painel criado com sucesso' }
        format.json { render :show, status: :created, location: @panel_page_index }
      else
        format.html { render :new }
        format.json { render json: @panel_page_index.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /panel_page_indices/1
  # PATCH/PUT /panel_page_indices/1.json
  def update
    respond_to do |format|
      if @panel_page_index.update(panel_page_index_params)
        format.html { redirect_to @panel_page_index, notice: 'Painel atualizado com sucesso' }
        format.json { render :show, status: :ok, location: @panel_page_index }
      else
        format.html { render :edit }
        format.json { render json: @panel_page_index.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /panel_page_indices/1
  # DELETE /panel_page_indices/1.json
  def destroy
    @panel_page_index.destroy
    respond_to do |format|
      format.html { redirect_to panel_page_indices_url, notice: 'Painel deletado com sucesso' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_panel_page_index
      @panel_page_index = PanelPageIndex.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def panel_page_index_params
      params.require(:panel_page_index).permit(:title_panel, :body_panel)
    end
end
