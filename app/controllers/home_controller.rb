class HomeController < ApplicationController
  def home
    @panels = PanelPageIndex.paginate(:page => params[:page], :per_page => 6).order(id: :desc)
  end

  def about
  end

  def services
  end

  def contact
    @contact = PageContact.first
  end

  def portfolio
  end
end
